# Relatório de entrega - Delivery Report
Me deparei com muitos problemas ao tentar subir a aplicação pelo serverless. Especificamente, o layer não estava reconhecendo o caminho do diretório "build". Como o tempo estava curto, foquei em construir as lambda functions e desenvolver as rotas, mesmo que as criando manualmente no dashboard da AWS.

# Base URL
https://oqberh00d3.execute-api.us-east-2.amazonaws.com

# Available API routes
## create product
Method: PUT or POST

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products

Body:
```json
{
  "_id": "4",
  "name": "Shoyu",
  "description": "Garrafa de Shoyu, 300ml.",
  "category": "condimentos",
  "brand": "Sakura",
  "price": "15,99",
  "inventory": {
    "total": "100",
    "available": "50"
  },
  "images": [],
  "created_at": "2023-04-15",
  "updated_at": "2023-04-15"
}
```

## Get all products
Method: GET

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products

## Get single product (by _id)
Method: GET

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products/{_id}

## Update single product (by _id)
Method: PUT

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products/{_id}

Body:
```json
{
  "name": "nome_atualizado",
  "description": "This is a test market product ATT",
  "category": "test-products",
  "brand": "test-a-lot",
  "price": "1337",
  "inventory": {
    "total": "70",
    "available": "20"
  }
}
```

## Delete single product (by _id)
Method: DELETE

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products/{_id}

## Insert image into a single product (by id and image file name)
Method: POST

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products/media/{_id}/{file_name}

Body:
```json
{

}
```
Importante mandar o body, mesmo que vazio.

## Delete image from a single product (by id and image file name)
Method: DELETE

URL: https://oqberh00d3.execute-api.us-east-2.amazonaws.com/products/media/{_id}/{file_name}

Body:
```json
{

}
```
Importante mandar o body, mesmo que vazio.


# Public png files in the bucket:
https://nina-backend-test-general-bucket.s3.us-west-2.amazonaws.com/ketchup.png

https://nina-backend-test-general-bucket.s3.us-west-2.amazonaws.com/maionese.png

https://nina-backend-test-general-bucket.s3.us-west-2.amazonaws.com/mostarda.png

https://nina-backend-test-general-bucket.s3.us-west-2.amazonaws.com/shoyu.png

# Especificações
- Todas as rotas que recebem _id retornam 404 caso um produto com esse _id não seja encontrado;
- As rotas que recebem Body (Create e Update) retornam 400 (bad request) caso recebam um atributo que não existe no product_schema;
- Um bucket foi criado e seus itens tornados públicos (mesmo não sendo o ideal), para facilidade nas funções de criar apagar mídias;
- Na inserção e remoção de mídias, o _id, bem como o nome da mídia, são procurados antes das ações para garantir que existem;
- A API identifica e não permite _id's duplicados, retornando 409 (Conflict).

# Melhorias
Com mais tempo, eu implementaria as seguintes mudanças:
1. Uso da função abort em casos de problemas específicos (como _id não encontrado ou duplicado). O uso efetivo dessa função torna as rotas bem mais legíveis pois interrompe a execução do código. Dessa forma, não seria necessário encadear vários if/elses em sequência para tratar casos problemáticos, apenas interromper a execução no momento da detecção.

2. Tentar subir o serverless. Teria tornado a produção de código bem mais simples e efetiva, mas empaquei no problema do layer não conseguir identificar o path do diretório build.

3. Criar políticas de acesso ao bucket. O bucket atualmente está com acesso público, mas isso está longe de ser o ideal.

# Back-end test - deadline 10/04 at 12h
As part of the interview process, we would like you to build a CRUD application. This will allow us to evaluate your understanding of Python and AWS services.

**You should send the repository link to team@ninamob.com before the deadline, even if it's incomplete.**

## Dependencies
|  Package   |  Version  |
| :--------: | :-------: |
|   python   | >= 3.8 |

## Test

1. Build and deploy an AWS API using the Serverless Framework, Lambda functions, and DynamoDB for a CRUD system that manages market products. **It's very important to show your knowledge of the Serverless Framework**.

2. The products should have media attachments. Create and Delete media endpoints should be included, and uploaded media should be stored on an S3 Bucket, with the URL saved in the product's 'medias' array field.
    * Obs: The [Create product](/src/api/create_product.py) and [Insert Media](src/api/insert_media.py) functions have already been implemented as examples, but you may need to modify them to fit your specific requirements.

3. Each product must have the following schema, and you have to make sure the API doesn't allow different objects and that it returns the proper HTTP codes:

```json
Product object schema

{
  "_id": "string",
  "name": "string",
  "description": "string",
  "category": "string",
  "brand": "string",
  "price": "number",
  "inventory": {
    "total": "number",
    "available": "number"
  },
  "images": ["string"],
  "created_at": "Date",
  "updated_at": "Date"
}
```

## Deployment

In order to deploy your application to the AWS Cloud, simply run:

```
make build
make deploy
```

When you are done with the challenge, please remove the resources and clean the stack by running:

```
make remove
make clean
```

## Recommended tools

We encourage you to read the documentation of the tools used in this task:

- [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
- [Serverless](https://www.serverless.com/framework/docs/)

## **Environment**

### Serverless Framework

### Dependencies
|  Package   |  Version  |
| :--------: | :-------: |
|   serverless | latest |

### Setting up Serverless

1. Install ```serverless framework```: https://www.serverless.com/framework/docs/getting-started

### AWS
You are required to create an [AWS account](https://aws.amazon.com) if you don't have one yet.

### Dependencies
|  Package   |  Version  |
| :--------: | :-------: |
|   aws-cli   |  latest |

### Setting up AWS

1. Install ```aws-cli```: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
2. Configure credentials on your local machine, by running:

```bash
$ aws configure
AWS Access Key ID [None]: <Your-Access-Key-ID>
AWS Secret Access Key [None]: <Your-Secret-Access-Key-ID>
Default region name [None]: us-west-1
Default output format [None]: json
```
View [CLI Config](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) for reference.

3. Create AWS credentials including the following IAM policies: ```AWSLambdaFullAccess```, ```AmazonS3FullAccess```, ```AmazonAPIGatewayAdministrator``` and ```AWSCloudFormationFullAccess```.

Run:
```sh
aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AWSLambda_FullAccess --user-name <username>

aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --user-name <username>

aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AmazonAPIGatewayAdministrator --user-name <username>

aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AWSCloudFormationFullAccess --user-name <username>
```