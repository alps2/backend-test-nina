import json
import boto3
from http import HTTPStatus

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('nina-market-products')

product_schema = [
  "_id",
  "name",
  "description",
  "category",
  "brand",
  "price",
  "inventory",
  "images",
  "created_at",
  "updated_at"
]


def obey_schema(new_product: dict):
    for key in new_product.keys():
        if key == "inventory":
            for inv_key in new_product[key].keys():
                if inv_key not in ["total", "available"]:
                    return False, inv_key
        if key not in product_schema:
            return False, key
    return True, ""

def lambda_handler(event, context):

    new_product = json.loads(event['body'])
    
    obey, bad_key = obey_schema(new_product)
    if obey:
        response = table.get_item(Key={ "_id": new_product["_id"] })
        if "Item" not in response:
            response = table.put_item(Item=new_product)
            status = HTTPStatus.CREATED
        else:
            response = { "error": "_id already in use." }
            status = HTTPStatus.CONFLICT
    else:
        response = {"error": f"{bad_key} does not exist in the product schema."}
        status = HTTPStatus.BAD_REQUEST

    return {
        'isBase64Encoded': False,
        'statusCode': status,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
