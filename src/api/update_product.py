import json
import boto3
from http import HTTPStatus
from datetime import date
import botocore

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('nina-market-products')

product_schema = [
  "_id",
  "name",
  "description",
  "category",
  "brand",
  "price",
  "inventory",
  "images",
  "created_at",
  "updated_at"
]

def obey_schema(new_product: dict):
    for key in new_product.keys():
        if key == "inventory":
            for inv_key in new_product[key].keys():
                if inv_key not in ["total", "available"]:
                    return False, inv_key
        if key not in product_schema:
            return False, key
    return True, ""

def lambda_handler(event, context):
    
    updated_product = json.loads(event['body'])
    
    obey, bad_key = obey_schema(updated_product)
    if not obey:
        response = {"error": f"{bad_key} does not exist in the product schema."}
        status = HTTPStatus.BAD_REQUEST
    else:
        _id = event['pathParameters']['_id']
    
        keys = { "_id": _id }
    
        update_expression = """
        SET
        #n = :new_name,
        description = :new_description,
        category = :new_category,
        brand = :new_brand,
        price = :new_price,
        inventory = :new_inventory,
        updated_at = :update_time
        """
    
        expression_names = {
            "#n": "name",
            "#_id": "_id"
        }
    
        expression_values = {
            ":_id": _id,
            ":new_name": updated_product["name"],
            ":new_description": updated_product["description"],
            ":new_category": updated_product["category"],
            ":new_brand": updated_product["brand"],
            ":new_price": updated_product["price"],
            ":new_inventory": updated_product["inventory"],
            ":update_time": str(date.today())
        }
        
        try:
            response = table.update_item(
                Key=keys,
                ConditionExpression="#_id = :_id",
                UpdateExpression=update_expression,
                ExpressionAttributeNames=expression_names,
                ExpressionAttributeValues=expression_values,
                ReturnValues='ALL_NEW'
            )
            status = HTTPStatus.OK
        except botocore.exceptions.ClientError as x:
            errorCode = x.response.get("Error", {}).get("Code")
            if errorCode == "ConditionalCheckFailedException":
                response = {"error": "_id not found."}
                status = HTTPStatus.NOT_FOUND
            else:
                response = {"error": "Something went wrong."}
                status = HTTPStatus.INTERNAL_SERVER_ERROR

    return {
        'isBase64Encoded': False,
        'statusCode': status,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
