import json
import boto3
import botocore
from http import HTTPStatus

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('nina-market-products')

BUCKET = "nina-backend-test-general-bucket"

def lambda_handler(event, context):
    
    _id = event['pathParameters']['_id']
    file_name = event['pathParameters']['file_name']
    
    url = f'https://{BUCKET}.s3.us-west-2.amazonaws.com/{file_name}'
    
    print(url)
    
    keys = { "_id": _id }
    
    update_expression = 'SET images = list_append(images, :image)'

    expression_names = {
        "#_id": "_id"
    }

    expression_values = {
        ":_id": _id,
        ':image': [url]
    }

    try:
        response = table.update_item(
            Key=keys,
            ConditionExpression="#_id = :_id",
            UpdateExpression=update_expression,
            ExpressionAttributeNames=expression_names,
            ExpressionAttributeValues=expression_values,
            ReturnValues='ALL_NEW'
        )
        status = HTTPStatus.OK
    except botocore.exceptions.ClientError as x:
        errorCode = x.response.get("Error", {}).get("Code")
        if errorCode == "ConditionalCheckFailedException":
            response = {"error": "_id not found."}
            status = HTTPStatus.NOT_FOUND
        else:
            print(errorCode)
            response = {"error": "Something went wrong."}
            status = HTTPStatus.INTERNAL_SERVER_ERROR

    return {
        'isBase64Encoded': False,
        'statusCode': status,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }