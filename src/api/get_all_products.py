import json
import boto3
from http import HTTPStatus

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('nina-market-products')

def lambda_handler(event, context):

    products = table.scan()
    response = products['Items']

    return {
        'isBase64Encoded': False,
        'statusCode': HTTPStatus.OK,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
