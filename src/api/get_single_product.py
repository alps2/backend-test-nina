import json
import boto3
from http import HTTPStatus

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('nina-market-products')

def lambda_handler(event, context):

    _id = event['pathParameters']['_id']
    
    keys = { "_id": _id }

    response = table.get_item(Key=keys)
    
    status = HTTPStatus.OK
    if 'Item' not in response:
        status = HTTPStatus.NOT_FOUND
        
    return {
        'isBase64Encoded': False,
        'statusCode': status,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
